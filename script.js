function tabuada() {
    var n = window.document.querySelector('input#num')
    var sel = window.document.querySelector('select#sel')
    if (n.value.length == 0) {
        window.alert("Por favor, digite um número!")
    } else {
        sel.innerHTML = `<option value="0">Tabuada</option>`
        for (var i = 1; i <= 10; i++) {
            var res = i * Number(n.value)
            sel.innerHTML += `<option value="${i}">${n.value}x${i}=${res}</option>`
        }

    }
}
